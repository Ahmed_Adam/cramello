//
//  OrdersModel.swift
//  Cramello
//
//  Created by Adam on 12/6/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//



import Foundation
import  SwiftyJSON




//            "grand_total"         : "70.00",
//            "order_date"          : "2017-12-03 13:07:39",
//            "order_id"            : "100000002",
//            "order_status"        : "processing",
//            "order_invoice"       : "100000003"


public class OrderModel {
    public var grand_total : Double?
    public var order_date : String?
    public var order_id : String?
    public var order_status : String?
    public var order_invoice : Int?
    
    
    

    public init?(dictionary: JSON) {

        grand_total = dictionary["grand_total"].double
        order_date = dictionary["order_date"].string
        order_id = dictionary["order_id"].string
        order_status = dictionary["order_status"].string
        order_invoice = dictionary["order_invoice"].int
    }


    
}
