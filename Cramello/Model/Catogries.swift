//
//  Catogries.swift
//  Cramello
//
//  Created by Adam on 12/17/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CatogriesModel  {
    var category_id : String?
    var name : String?
    var parent_id : String?
    var active : String?
    var level : String?
    var position : String?
    var thumbnail : String?
    var children : String?
    var products : [ProductModel]?
    
    public init?(dictionary: JSON) {
        
        category_id = dictionary["category_id"].string
        name = dictionary["name"].string
        parent_id = dictionary["parent_id"].string
        active = dictionary["active"].string
        level = dictionary["level"].string
        position = dictionary["position"].string
        thumbnail = dictionary["thumbnail"].string
        children = dictionary["children"].string
        products = dictionary["products"].arrayValue as? [ProductModel]
    }
    
}
