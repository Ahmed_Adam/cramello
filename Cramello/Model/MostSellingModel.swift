//
//  MostSellingModel.swift
//  Cramello
//
//  Created by Adam on 12/7/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation
import  SwiftyJSON


//            "type"        :  "most_selling",
//            "page"        :   1,
//            "limit"       :   10

public class MostSellingModel {
    
    public var type :  String?
    public var page : Int?
    public var limit : Int?
    
    
    public init?(dictionary: JSON) {
        
        
        type = dictionary["type"].string
        page = dictionary["page"].int
        limit = dictionary["limit"].int
    }
    
}

