//
//  OrderDetails.swift
//  Cramello
//
//  Created by Adam on 12/9/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation
import  SwiftyJSON



public class OrderDetailsModel {
    public var order_id : Int?
    public var created_at : String?
    public var customer_name : String?
    public var grand_total : Double?
    public var status_label : String?
    public var shipping_method : String?
    public var address : AddressListModel?
  //  public var pay_method : Pay_method?
 //   public var order_currency : Order_currency?
    public var subtotal : Double?
    public var shipping_amount : Double?
    public var products : Array<ProductModel>?
 //   public var invoice : Invoice?
    

    public init?(dictionary: JSON) {

        
        order_id = dictionary["order_id"].int
        created_at = dictionary["created_at"].string
        customer_name = dictionary["customer_name"].string
        grand_total = dictionary["grand_total"].double
        status_label = dictionary["status_label"].string
        shipping_method = dictionary["shipping_method"].string
    //    if (dictionary["address"] != nil) { address = Address(dictionary: dictionary["address"] as! NSDictionary) }
   //     if (dictionary["pay_method"] != nil) { pay_method = Pay_method(dictionary: dictionary["pay_method"] as! NSDictionary) }
   //     if (dictionary["order_currency"] != nil) { order_currency = Order_currency(dictionary: dictionary["order_currency"] as! NSDictionary) }
        subtotal = dictionary["subtotal"].double
        shipping_amount = dictionary["shipping_amount"].double
  //      if (dictionary["products"] != nil) { products = Products.modelsFromDictionaryArray(dictionary["products"] as! NSArray) }
  //      if (dictionary["invoice"] != nil) { invoice = Invoice(dictionary: dictionary["invoice"] as! NSDictionary) }
    }
    
    

    
}
