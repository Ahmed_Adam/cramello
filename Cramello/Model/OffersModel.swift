//
//  OffersModel.swift
//  Cramello
//
//  Created by Adam on 12/20/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation
import SwiftyJSON

class OffersModel {
    
    var entityId : String?
    var imageUrl : String?
    var isSaleable : String?
    var name : String?
    var price : String?
    var shortDescription : String?
    var sku : String?
    var specialPrice : String?
    


public init?(dictionary: JSON) {
    
        entityId = dictionary["entityId"].string
        name = dictionary["name"].string
        imageUrl = dictionary["imageUrl"].string
        isSaleable = dictionary["isSaleable"].string
        price = dictionary["price"].string
        shortDescription = dictionary["shortDescription"].string
        sku = dictionary["sku"].string
        specialPrice = dictionary["specialPrice"].string
    }
}
