//
//  AddressListModel.swift
//  Cramello
//
//  Created by Adam on 12/12/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation
import  SwiftyJSON


//
//                "model": [
//                {
//                "customer_id": "80",
//                "id": "63",
//                "addressName": "addressName",
//                "addressType": "addressType",
//                "block": "block",
//                "buildingNo": "buildingNo",
//                "area": "area",
//                "mobile": "mobile",
//                "street": "street",
//                "apartmentNo": "apartmentNo",
//                "floor": "floor",
//                "extraDeleveryInformation": "extraDeleveryInformation",
//                "is_default_billing": false,
//                "is_default_shipping": false
//                }


public class AddressListModel {
    
    public var customer_id : String?
    public var id : String?
    public var addressName : String?
    public var addressType : String?
    public var block : String?
    public var buildingNo : String?
    public var area : String?
    public var mobile : String?
    public var street : String?
    public var apartmentNo : String?
    public var floor : String?
    public var extraDeleveryInformation : String?
    public var is_default_billing : Bool?
    public var is_default_shipping : Bool?
    
    
    
    public init?(dictionary: JSON) {
        
        customer_id = dictionary["customer_id"].string
        id = dictionary["id"].string
        addressName = dictionary["addressName"].string
        area = dictionary["area"].string
        addressType = dictionary["addressType"].string
        block = dictionary["block"].string
        street = dictionary["street"].string
        apartmentNo = dictionary["apartmentNo"].string
        buildingNo = dictionary["buildingNo"].string
        floor = dictionary["floor"].string
        mobile = dictionary["mobile"].string
        extraDeleveryInformation = dictionary["extraDeleveryInformation"].string
        is_default_shipping = dictionary["is_default_shipping"].bool
        is_default_billing = dictionary["is_default_billing"].bool
        
        
        
        
    }
    
    
    
}

