//
//  CrtModel.swift
//  Cramello
//
//  Created by Adam on 12/7/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation
import  SwiftyJSON


//            "customer_id"     :80,
//            "product_id"      :[1,2],
//            "qty"             :[1,1]


public class CartModel {

    public var customer_id : Int?
    public var product_id : Int?
    public var qty : Int?


    public init?(dictionary: JSON) {


        customer_id = dictionary["customer_id"].int
        product_id = dictionary["product_id"].int
        qty = dictionary["qty"].int
    }

}


