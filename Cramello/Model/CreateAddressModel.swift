


import Foundation
import SwiftyJSON


struct CreateAddressModel  {
    let customer_id : String?
    let id : String?
    let addressName : String?
    let area : String?
    let addressType : String?
    let block : String?
    let street : String?
    let apartmentNo : String?
    let buildingNo : String?
    let floor : String?
    let mobile : String?
    let extraDeleveryInformation : String?
    
    public init?(dictionary: JSON) {
        
        customer_id = dictionary["customer_id"].string
        id = dictionary["id"].string
        addressName = dictionary["addressName"].string
        area = dictionary["area"].string
        addressType = dictionary["addressType"].string
        block = dictionary["block"].string
        street = dictionary["street"].string
        apartmentNo = dictionary["apartmentNo"].string
        buildingNo = dictionary["buildingNo"].string
        floor = dictionary["floor"].string
        mobile = dictionary["mobile"].string
        extraDeleveryInformation = dictionary["extraDeleveryInformation"].string
      
        
        
        
        
    }
    
}
