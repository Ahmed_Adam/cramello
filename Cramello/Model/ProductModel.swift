//
//  ProductModel.swift
//  Cramello
//
//  Created by Adam on 12/7/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation
import  SwiftyJSON



//                    "entity_id"         :    "2",
//                    "sku"               :    "0002",
//                    "name"              :    "Four Seasons Cake",
//                    "short_description" :    "Our best, strongest separator plates
//                                              with
//                                              superior stability, beauty and l  ovely
//                                                 scalloped edges. Circles of strength
//                                                construction evenly supports all area
//                                                 of
//                                                cake. Guaranteed non-breakable.
//                                                  \r\n\r\n",
//                    "image_url"          :    "http://cramello.com/media/catalog/
//                                               product/cache/1/image/265x/
//                                               9df78eab33525d08d6e5fb8d2713
//                                                6e95/0/2/02.jpg",
//                    "price"              :    "12.00",
//                    "special_price"      :    "10.00",
//                    "is_saleable"        :    "1"

public class ProductModel {
 
    public var entity_id : String?
    public var sku : String?
    public var name : String?
    public var short_description : String?
    public var image_url : String?
    public var price : String?
    public var special_price : String?
    public var is_saleable : String?
    
    
    public init?(dictionary: JSON) {
        
        
        entity_id = dictionary["entity_id"].string
        sku = dictionary["sku"].string
        name = dictionary["name"].string
        short_description = dictionary["short_description"].string
        image_url = dictionary["image_url"].string
        price = dictionary["price"].string
        special_price = dictionary["special_price"].string
        is_saleable = dictionary["is_saleable"].string
        
    }

}
