//
//  design.swift
//  Imagetextfield
//
//  Created by Abdelrahman Ahmed Shawky on 11/12/17.
//  Copyright © 2017 Abdelrahman Ahmed Shawky. All rights reserved.
//

import UIKit

@IBDesignable
class design: UITextField {
    
    override func draw(_ rect: CGRect) {
        
        let borderLayer = CALayer()
        let widthOfBorder = getBorderWidht()
        borderLayer.frame = CGRect(x: -15, y: frame.size.height - widthOfBorder, width: frame.size.width + 20, height: frame.size.height)
        //borderLayer.frame = CGRect(x: -15, y: 0.0, width: self.frame.size.width+20, height: self.frame.size.height - widthOfBorder)
        borderLayer.borderWidth = widthOfBorder
        borderLayer.borderColor = getBottomLineColor()
        self.layer.addSublayer(borderLayer)
        self.layer.masksToBounds = true
        
    }
    
//    //MARK : set the image LeftSide
//    @IBInspectable var SideImage:UIImage? {
//        didSet{
//
//            let leftAddView = UIView.init(frame: CGRectMake(0, 0, 25, self.frame.size.height-10))
//            let leftimageView = UIImageView.init(frame: CGRectMake(0, 0, 20, 20))//Create a imageView for left side.
//            leftimageView.image = SideImage
//            leftAddView.addSubview(leftimageView)
//            self.leftView = leftAddView
//            self.leftViewMode = UITextFieldViewMode.Always
//        }
//
//    }
    @IBInspectable var bottomLineColor: UIColor = UIColor.black {
        didSet {
            
        }
    }
    
    
    func getBottomLineColor() -> CGColor {
        return bottomLineColor.cgColor;
        
    }
    @IBInspectable var cusborderWidth:CGFloat = 1.0
        {
        didSet{
            
        }
    }
    
    func getBorderWidht() -> CGFloat {
        return cusborderWidth;
        
    }
}


