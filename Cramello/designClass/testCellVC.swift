//
//  testCellVC.swift
//  register
//
//  Created by Abdelrahman Ahmed Shawky on 11/21/17.
//  Copyright © 2017 Abdelrahman Ahmed Shawky. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SkyFloatingLabelTextField
var user_number = [user]()
var _Status = ""

class testCellVC: UICollectionViewCell,UITextFieldDelegate{
    
    var controller: UIViewController?
    
    // MARK: - outlet
    @IBOutlet var btnDropGender: DropMenuButton!
    @IBOutlet var textfield: design!
    @IBOutlet var textfield_gender: SkyFloatingLabelTextField!
    @IBOutlet var txt_countryCode: SkyFloatingLabelTextField!
    @IBOutlet var Btn_country_outlet: DropMenuButton!
    // MAEK: - Name And Email
    @IBOutlet var firstNameText: SkyFloatingLabelTextField!
    @IBOutlet var middleNameText: SkyFloatingLabelTextField!
    @IBOutlet var lastNameText: SkyFloatingLabelTextField!
    @IBOutlet var userEmailText: SkyFloatingLabelTextField!
    // MARK:- Phone
    @IBOutlet var userPhoneText: SkyFloatingLabelTextField!
    // MARK:- Password
    @IBOutlet var userPasswordText: SkyFloatingLabelTextField!
    @IBOutlet var confirmPasswordText: SkyFloatingLabelTextField!
    
    
    // MARK: - all varible
    var genderArr = [ "Female","Male","Other"]
    var storedGender: Int = 0
    var countryList = [Country]()
    var countryArr = [String]()
    var country_id = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userEmailText.delegate = self
        // Initialization code
        Api.GetCountry { (message, countryList) in
            guard let dataArr = countryList else {
                return
            }
            self.countryList = dataArr
            for country in dataArr {
                self.countryArr.append(country.name)
            }
        }
    }
    
    
    
    // MARK:- Fuction registeration
    func Signup() {
        
        guard let firstName = firstNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines), !firstName.isEmpty  else {
            displayAlert(message: "please enter first name")
            return
        }
        
        guard let midName = middleNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines), !midName.isEmpty  else {
            displayAlert(message: "please enter middle name")
            return
        }
        
        guard let lastName = lastNameText.text?.trimmingCharacters(in: .whitespacesAndNewlines), !lastName.isEmpty  else {
            displayAlert(message: "please enter last name")
            return
        }
        
        guard let userPhone = userPhoneText.text?.trimmingCharacters(in: .whitespacesAndNewlines), !userPhone.isEmpty  else {
            displayAlert(message: "please enter phone number")
            return
        }
        
        guard let userEmail = userEmailText.text?.trimmingCharacters(in: .whitespacesAndNewlines), !userEmail.isEmpty  else {
            displayAlert(message: "please enter email")
            return
        }
        
        guard let userPassword = userPasswordText.text?.trimmingCharacters(in: .whitespacesAndNewlines), !userPassword.isEmpty  else {
            displayAlert(message: "please enter password")
            return
        }
        
        guard let confirmPassword = confirmPasswordText.text?.trimmingCharacters(in: .whitespacesAndNewlines), !confirmPassword.isEmpty  else {
            displayAlert(message: "please confirm password")
            return
        }
        
        guard userPassword == confirmPassword else {
            displayAlert(message: "please match password")
            return
        }
        
        guard let gender = textfield_gender.text?.trimmingCharacters(in: .whitespacesAndNewlines) , !gender.isEmpty else {
            displayAlert(message: "please select gender")
            return
        }
        
        guard let countryCode = txt_countryCode.text?.trimmingCharacters(in: .whitespacesAndNewlines), !countryCode.isEmpty  else {
            displayAlert(message: "please select country")
            return
        }
        
        Api.Register(firstName: firstName, midName: midName, lastName: lastName, phone: userPhone, email: userEmail, password: userPassword, gender: gender, countryId: self.country_id, countryCode: countryCode) { (message, status) in
            if let msg = message {
                self.displayAlert(message: msg)
                return
            }
            
            print("Done")
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let verifyVC = storyBoard.instantiateViewController(withIdentifier: "verify") as! Verify_Nummber
            verifyVC.oldPhone = countryCode + userPhone
            self.controller?.show(verifyVC, sender: nil)
            //self.displayAlert(message: "success")
        }
        
    }
    
    // MARK: - action
    @IBAction func btndropdown_gender(_ sender: DropMenuButton) {
        self.btnDropGender.initMenu(self.genderArr, actions: [({ () -> (Void) in
            self.btnDropGender.setTitle("", for: .normal)
            self.storedGender = self.btnDropGender.storegenderIndex
            print( self.storedGender)
            print("Estou fazendo a ação A")
            self.textfield_gender.text = "Female"
        }), ({ () -> (Void) in
            print("Estou fazendo a ação B")
            self.btnDropGender.setTitle("", for: .normal)
            self.textfield_gender.text = "Male"
        }), ({ () -> (Void) in
            print("Estou fazendo a ação C")
            self.btnDropGender.setTitle("", for: .normal)
            self.textfield_gender.text = "Other"
        })]
        )
    }
    
    @IBAction func btn_countryaction(_ sender: DropMenuButton) {
        self.Btn_country_outlet.initMenu(countryArr,actions: [({ () -> (Void) in
            self.Btn_country_outlet.setTitle("", for: .normal)
            self.txt_countryCode.text = "+966"
            self.country_id = "003acd42-da72-41dc-8c12-29402c8f443a"
            //
            print("Estou fazendo a ação A")
        }), ({ () -> (Void) in
            self.Btn_country_outlet.setTitle("", for: .normal)
            self.txt_countryCode.text = "+0"
            self.country_id = "5e52a86f-10d8-4004-b5a3-8ae0dd88c1db"
            print("Estou fazendo a ação B")
        }), ({ () -> (Void) in
            self.Btn_country_outlet.setTitle("", for: .normal)
            self.txt_countryCode.text = "+2"
            //921fb238-b555-4099-a92d-b8436a596de8
            self.country_id = "921fb238-b555-4099-a92d-b8436a596de8"
            print("Estou fazendo a ação C")
        })])
    }
    
    @IBAction func btn_verify_Mobile(_ sender: Any) {
        Signup()
    }
    
    private func displayAlert (message: String) {
        let alert = UIAlertController(title: "Warrning", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        controller?.present(alert, animated: true, completion: nil)
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
                if !isValidEmail(testStr: text) {
                    floatingLabelTextField.errorMessage = "Invalid email"
                } else {
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
         return true
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if let text = textField.text {
//            if let floatingLabelTextField = textField as? SkyFloatingLabelTextField {
//                if !isValidEmail(testStr: text) {
//                    floatingLabelTextField.errorMessage = "Invalid email"
//                }
//            }
//        }
//    }
    
}
