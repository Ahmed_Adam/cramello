//
//  CreateNewAddressTableVC.swift
//  Cramello
//
//  Created by Adam on 12/13/17var/  Copyright ©vavar aldar-int. All rivars reserved.
//

import UIKit
import SwiftyJSON
import MapKit
//import CoreLocation

class CreateNewAddressTableVC: UITableViewController  {

    @IBOutlet var tableview: UITableView!
   //  var locationManager: CLLocationManager!
    
    
    @IBOutlet weak var addressName: UITextField!
    @IBOutlet weak var area: UITextField!
    @IBOutlet weak var addressType: UITextField!
    @IBOutlet weak var block: UITextField!    
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var avenu: UITextField!
    @IBOutlet weak var buldingNo: UITextField!
    @IBOutlet weak var floor: UITextField!
    @IBOutlet weak var appartNo: UITextField!
    @IBOutlet weak var mobileNo: UITextField!
    @IBOutlet weak var extra: UITextField!
    
    
    var address : [String : String]?
    
    

    
    
    
    @IBAction func area(_ sender: UITextField) {
        let chooseArea = self.storyboard?.instantiateViewController(withIdentifier: "city") as! CityViewController
        self.present(chooseArea, animated: true, completion: nil)
        
        
    }
    @IBAction func choose(_ sender: UITextField) {
        
        
  
    }
    
  
    
    
    @IBAction func saveAddress(_ sender: UIButton) {
        
     createAddress ()
        let map = self.storyboard?.instantiateViewController(withIdentifier: "map") as! MapViewController
        self.present(map, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        


   
    }
    



    func createAddress (){
        
        address = [   "id" :  USER_ID.getUserID()! ,
                      "addressName" : self.addressName.text!,
                      "addressType" : addressType.text! ,
                      "apartmentNo" : appartNo.text!,
                          "area"        : "area.text!",
                          "street"      : street.text!,
                          "block"       : block.text! ,
                          "buildingNo"  : buldingNo.text! ,
                          "extraDeleveryInformation" : extra.text! ,
                          "floor"       : floor.text!,
                          "mobile"      : mobileNo.text! ]
        Web_Services.CreateAddress(address: address! , complition: { (error , success ) in
            
            if success {
                let map = self.storyboard?.instantiateViewController(withIdentifier: "map") as! MapViewController
                self.present(map, animated: true, completion: nil)
     
                }
          })
    }

}
