//
//  MenuTableViewCell.swift
//  Cramello
//
//  Created by Adam on 12/17/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Base Class Overrides
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
       
    }

}
