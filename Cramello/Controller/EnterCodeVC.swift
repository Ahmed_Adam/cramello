//
//  EnterCodeVC.swift
//  Cramello
//
//  Created by Adam on 12/3/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit

class EnterCodeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var codeText: UITextField!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func vack(_ sender: UIBarButtonItem) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func VerifyCode(_ sender: UIButton) {
        let code = codeText.text
        let vc = VerifyYourPHoneVC()
        let mobile = vc.phoneTextField.text

        Web_Services.Verify(code: code!, mobile: mobile!  ) { (error,success ) in
            if success{
                print ("succees")
                let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
                self.present(home, animated: true, completion: nil)

            }
        
        }
        
        
    }
    

}
