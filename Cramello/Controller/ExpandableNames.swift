//
//  ExpandableNames.swift
//  Cramello
//
//  Created by Adam on 12/13/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation


struct ExpandableNames {
    
    var isExpanded: Bool
    let names: [String]
    
}
