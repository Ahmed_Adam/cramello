//
//  MyAddressViewController.swift
//  Cramello
//
//  Created by Adam on 12/8/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit

class MyAddressViewController: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var allAddresses = [AddressListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        
        getMyAddressList()
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backHome(_ sender: Any) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
        
    }
    
    func  getMyAddressList(){
        
        Web_Services.MyAddressList(customer_code : 89)  { (error, success, addressArray) in
            
            if success{
                
                self.allAddresses = addressArray!
                
                print(self.allAddresses)
                self.tableview.reloadData()
                
            }
            else {
                return
            }
        }
            
            
        }
        
    }
    

   
    





extension MyAddressViewController: UITableViewDataSource, UITableViewDelegate {
    
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allAddresses.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyAddressCellVCTableViewCell

        let arrayOf_dataInJSON = allAddresses[indexPath.row]
        cell.addressName.text = arrayOf_dataInJSON.addressName
       cell.area.text = arrayOf_dataInJSON.area
        cell.code.text =  arrayOf_dataInJSON.buildingNo
        cell.mobile.text = arrayOf_dataInJSON.mobile
        
        
  
        return cell
    }
    
}
