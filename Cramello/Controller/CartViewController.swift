//
//  CartViewController.swift
//  Cramello
//
//  Created by Adam on 12/26/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    
    
    @IBOutlet weak var productView: UIView!
    
    @IBOutlet weak var productView2: UIView!
    
    @IBAction func deleteproduct(_ sender: UIButton) {
        
        productView2.isHidden = true
    }
    
    
    @IBAction func deleteProduct(_ sender: UIButton) {
        
        
        productView.isHidden = true
    }
    
    @IBOutlet weak var item: UILabel!
    @IBOutlet weak var item2: UILabel!
    
    var items = 0
    
    @IBAction func increment(_ sender: UIButton) {
        
        items = items + 1
        item.text = "\(items)"
    }
    
    @IBAction func decrament(_ sender: UIButton) {
        if ( items > 0) {
            items = items - 1
            item.text = "\(items)"
            
        }
    }
    
    @IBAction func increment1(_ sender: UIButton) {
        
        items = items + 1
        item2.text = "\(items)"
    }
    
    @IBAction func decrament2(_ sender: UIButton) {
        if ( items > 0) {
            items = items - 1
            item2.text = "\(items)"
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


}
