//
//  OrderDetailsVCViewController.swift
//  Cramello
//
//  Created by Adam on 12/10/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OrderDetailsVCViewController: UIViewController , NVActivityIndicatorViewable {

      var   orderDetails = [OrderDetailsModel]()
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: sender.tag)!)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("your Addresses are ready...")
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getOrderDetails ()
        

        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var order_id: UILabel!
    @IBOutlet weak var address_name: UILabel!
    @IBOutlet weak var block: UILabel!
    @IBOutlet weak var street: UILabel!
    
    @IBOutlet weak var house: UILabel!
    
    @IBOutlet weak var extra: UILabel!
    
    @IBOutlet weak var paymentType: UILabel!
    
    func  getOrderDetails ()  {
        
        Web_Services.MyOrdersDetails { (error, true, orderDetails) in
            
            if orderDetails != nil {
                self.orderDetails = orderDetails!
                
                
                
                
                print(" all orders  ::::::::  \(self.orderDetails)")
            }
            else {
                print (error ?? 0)
            }
            
            
        }
        
        
    }
    
    
    
    
    
    
}
