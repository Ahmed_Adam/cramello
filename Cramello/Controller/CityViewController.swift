//
//  CityViewController.swift
//  Cramello
//
//  Created by Adam on 12/17/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit

class CityViewController: UIViewController
, UITableViewDelegate , UITableViewDataSource , UISearchBarDelegate
{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.cityChosed = areaArray[indexPath.row]
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching{
            return filteredData.count
        }
        return 8
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.areaArray.append(contentsOf:  ["Kuwait City (Capital)",
                                            " Abdullah Al-Salem " ,
                                            " Adailiya ",
                                            " Bneid Al Qar ",
                                            " Daiya ",
                                            " Dasma ",
                                            " Doha ",
                                            " Faiha "])
        
       if let cell = tableview.dequeueReusableCell(withIdentifier: "cell") as? areaTableViewCell {
                let text: String!
                cell.selectionStyle = UITableViewCellSelectionStyle.gray
        cell.isUserInteractionEnabled = true
                if isSearching {
                    
                    text = filteredData[indexPath.row]
                    
                } else {
                    
                    text = areaArray[indexPath.row]
                }
                
                cell.areaLabel.text = text
                
                return cell
                
            } else {
                
                return UITableViewCell()
            }
        
    
    }
    
    

    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var tableview: UITableView!
    var isSearching  = false
    var filteredData = [String]()
    
    var cityChosed : String!
    
    var areaArray = Array<String>()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        
        search.delegate = self
        search.returnKeyType = UIReturnKeyType.done
        
        getArea()

        // Do any additional setup after loading the view.
    }

    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == nil || searchBar.text == "" {
            
            isSearching = false
            
            view.endEditing(true)
            
            tableview.reloadData()
            
        } else {
            
            isSearching = true
            
            filteredData = areaArray.filter({$0 == searchBar.text})
            
            tableview.reloadData()
        }
    }
    
  
    func getArea(){
        
        Web_Services.GetArea { (error, success, allArea) in
        
            self.areaArray = allArea!
            print (self.areaArray.count)
        }
    }


}
