//
//  MapViewController.swift
//  Cramello
//
//  Created by Adam on 12/18/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import MapKit





class MapViewController: UIViewController  , MKMapViewDelegate {
    
    
    
    @IBOutlet weak var mapview: MKMapView!
    



    
    
    @IBAction func zoom(_ sender: UIButton) {
        
        
        let location = mapview.userLocation
        let region = MKCoordinateRegionMakeWithDistance((location.location?.coordinate)!, 2000, 2000)
        
        mapview.setRegion(region, animated: true)
        
        
    }
  
    @IBAction func saveAddress(_ sender: UIButton) {
        // Create the alert controller
        let alertController = UIAlertController(title: "Confirm Location ", message: "Please Ensure your address is acurate and complete ", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
            
            let addressEnsure = self.storyboard?.instantiateViewController(withIdentifier: "AddressEnsure") 
            self.present(addressEnsure!, animated: true, completion: nil)
            
            NSLog("OK Pressed")
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        mapview.showsUserLocation = true
        mapview.delegate = self
        
        
    }




}










