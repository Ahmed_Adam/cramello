//
//  ChoicesViewController.swift
//  Cramello
//
//  Created by Adam on 12/25/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import UICheckbox

//import TKAnimatedCheckButton

class ChoicesViewController: UIViewController {
    
    
    @IBOutlet weak var chocolateImage: UIImageView!
    
    @IBOutlet weak var vanillaImage: UIImageView!
    
    @IBOutlet weak var halfandhalf: UIImageView!
    
    @IBOutlet weak var layerwithflake: UIImageView!
    
    @IBAction func clickChocolate(_ sender: UIButton) {
        
        chocolateImage.isHidden = false
        vanillaImage.isHidden = true
        halfandhalf.isHidden = true
        layerwithflake.isHidden = true
    }
    
    @IBAction func clickVanilla(_ sender: UIButton) {
        chocolateImage.isHidden = true
        vanillaImage.isHidden = false
        halfandhalf.isHidden = true
        layerwithflake.isHidden = true
    }
    
    @IBAction func halfAndHalf(_ sender: UIButton) {
        
        chocolateImage.isHidden = true
        vanillaImage.isHidden = true
        halfandhalf.isHidden = false
        layerwithflake.isHidden = true
    }
    
    @IBAction func layerwithflake(_ sender: UIButton) {
        
        chocolateImage.isHidden = true
        vanillaImage.isHidden = true
        halfandhalf.isHidden = true
        layerwithflake.isHidden = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chocolateImage.isHidden = true
      vanillaImage.isHidden = true
        halfandhalf.isHidden = true
        layerwithflake.isHidden = true
        
    }
    

    
}

