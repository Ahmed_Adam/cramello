//
//  OrdersVC.swift
//  Cramello
//
//  Created by Adam on 12/4/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class OrdersVC: UIViewController , NVActivityIndicatorViewable{
    
    
    @IBOutlet weak var orderTable: UITableView!

  var allOrders = [OrderModel]()
    
    override func viewDidAppear(_ animated: Bool) {
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...")
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading ...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.stopAnimating()
      }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
        orderTable.delegate = self
        orderTable.register(UITableViewCell.self, forCellReuseIdentifier: "OrederCell")
        orderTable.dataSource = self as UITableViewDataSource
        
        
        
        
        
        getMyOrders()
        

        // Do any additional setup after loading the view.
    }


    

    @IBAction func BacktoHome(_ sender: UIBarButtonItem) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
        
    }

    
    func getMyOrders(){
        
        Web_Services.MyOrders { (error, success, allorders) in
            if allorders != nil {
                self.allOrders = allorders!
                self.orderTable.reloadData()
               // print(self.allOrders)
                
            }
        }
        
        
        
    }

}



extension OrdersVC : UITableViewDelegate, UITableViewDataSource{
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell =  tableView.dequeueReusableCell(withIdentifier: "OrederCell", for: indexPath)
        
        cell.textLabel?.text = allOrders[indexPath.row].order_date
        
       return cell
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    

    
   
    
}
