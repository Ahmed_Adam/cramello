//
//  SideMenuVC.swift
//  Side Menu
//
//  Created by Kyle Lee on 8/6/17.
//  Copyright © 2017 Kyle Lee. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class SideMenuVC: UITableViewController , NVActivityIndicatorViewable {

   
    
    
    @IBAction func HomeButton(_ sender: UIButton) {
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
    }
    
    @IBAction func OffersButton(_ sender: UIButton) {
        
        let offers = self.storyboard?.instantiateViewController(withIdentifier: "Offers") as! OffersVC
        self.show(offers, sender: self)
        
    }
    
    @IBAction func OrdersButton(_ sender: UIButton) {
         let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: (sender as AnyObject).tag)!)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading ...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
            self.stopAnimating()
        }
        
    
        
    }
    @IBOutlet weak var addressbutton: UIButton!
    
    @IBAction func AddressButton(_ sender: UIButton) {
        
        let vc = HomeVC()
        
        vc.ChooseAddress(self.addressbutton)
    //    performSegue(withIdentifier: "GoToAddress", sender: self)
        
//        let address = self.storyboard?.instantiateViewController(withIdentifier: "MyAddress") as! MyAddressViewController
//        self.present(address, animated: true, completion: nil)
        
        
    }
    
    @IBAction func ProfileButton(_ sender: UIButton) {
        
        let profile = self.storyboard?.instantiateViewController(withIdentifier: "Profile") as! ProfileVC
        self.present(profile, animated: true, completion: nil)
    }
    
    @IBAction func NotificationButton(_ sender: UIButton) {
        
//        let myNoti = self.storyboard?.instantiateViewController(withIdentifier: "MyNotification") as! NotificationVC
//        self.present(myNoti, animated: true, completion: nil)
    }
    
    @IBAction func ContactUsButton(_ sender: UIButton) {
        let contactUs = self.storyboard?.instantiateViewController(withIdentifier: "ContactUs") as! ContactUsVC
        self.present(contactUs, animated: true, completion: nil)
        
    }
    
    @IBAction func SahreAppButton(_ sender: UIButton) {
        
       
    }
    
    
    override func viewDidLoad() {
        
    }

}
