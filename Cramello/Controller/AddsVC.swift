//
//  ViewController.swift
//  Cramello
//
//  Created by Adam on 12/3/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire

class AddsVC: UIViewController {
  
    
    @IBOutlet var sliderView: ImageSlideshow!
    var arrayImages = [ImageSource]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getOffers()
       
        sliderView.backgroundColor = UIColor.white
        sliderView.slideshowInterval = 5.0
        sliderView.pageControlPosition = PageControlPosition.underScrollView
        sliderView.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        sliderView.pageControl.pageIndicatorTintColor = UIColor.black
        sliderView.contentScaleMode = UIViewContentMode.center
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        sliderView.activityIndicator = DefaultActivityIndicator()
        sliderView.currentPageChanged = { page in
            print("current page:", page)
        }
        
    
        sliderView.setImageInputs(arrayImages)
        
        
        }
        
    
    func getOffers(){
        
        Web_Services.GetOffers { (error, success , allOffers ) in
            
            for offerimage in allOffers! {
                
              
            
                Alamofire.request( offerimage.imageUrl! ).responseData { (response ) in
                    self.arrayImages.append(ImageSource(image:UIImage(data: response.data!)!))
                    self.sliderView.setImageInputs(self.arrayImages)
                    print (response.data as Any)

                }
            }
            

        }
    //require method

    // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
              
        
     //   self.sliderView.setImageInputs(arrayImages as! [InputSource])
    // sliderView.setImageInputs(sdWebImageSource as! [InputSource])
    
    let recognizer = UITapGestureRecognizer(target: self, action: #selector(OffersVC.didTap))
    sliderView.addGestureRecognizer(recognizer)
    
    
}
    

    @objc func didTap() {
        let fullScreenController = sliderView.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    
   
    
    @IBAction func home(_ sender: UIButton) {
        
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
        
    }


}

