//
//  areaTableViewCell.swift
//  Cramello
//
//  Created by Adam on 12/21/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit

class areaTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var area: UIView!
    @IBOutlet weak var areaLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
