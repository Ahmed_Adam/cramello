//
//  MainVC.swift
//  Side Menu
//
//  Created by Kyle Lee on 8/6/17.
//  Copyright © 2017 Kyle Lee. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class HomeVC: UIViewController , NVActivityIndicatorViewable {
    
    @IBOutlet weak var myOffers: UIButton!
    
    @IBOutlet weak var chooseDeleveryButton: UIView!
    
    @IBAction func order(_ sender: Any) {
        
        
        
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: (sender as AnyObject).tag)!)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading ...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5) {
            self.stopAnimating()
        }
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "MyOrder") as! OrdersTableViewController
        self.show(order, sender: self)
        
    }
    @IBAction func ChooseAddress(_ sender: UIButton) {
        
        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: sender.tag)!)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading ...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.stopAnimating()
        }
        
    
    
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        myOffers.layer.borderWidth = 2
        myOffers.layer.borderColor = UIColor.white.cgColor
 
       
       
    }
  
    
    @IBAction func onMoreTapped() {
        print("TOGGLE SIDE MENU")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
    }
    
    

}
