//
//  ProfileVC.swift
//  Cramello
//
//  Created by Adam on 12/4/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ProfileVC: UIViewController ,UITextFieldDelegate {
    
     var genderArr = [ "Female","Male","Other"]
    var storedGender: Int = 0
    @IBOutlet var btnDropGender: DropMenuButton!
    @IBOutlet weak var textfield_gender: SkyFloatingLabelTextField!
    
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var firstName: UITextField!
    
    @IBOutlet weak var lastName: UITextField!
    
    @IBOutlet weak var gender: UIButton!
    
    @IBAction func btndropdown_gender(_ sender: DropMenuButton) {
        self.btnDropGender.initMenu(self.genderArr, actions: [({ () -> (Void) in
            self.btnDropGender.setTitle("", for: .normal)
            self.storedGender = self.btnDropGender.storegenderIndex
            print( self.storedGender)
            print("Estou fazendo a ação A")
            self.btnDropGender.setTitle("Female", for: .normal)
            self.textfield_gender.text = "Female"
        }), ({ () -> (Void) in
            print("Estou fazendo a ação B")
            self.btnDropGender.setTitle("", for: .normal)
            self.textfield_gender.text = "Male"
        }), ({ () -> (Void) in
            print("Estou fazendo a ação C")
            self.btnDropGender.setTitle("", for: .normal)
            self.textfield_gender.text = "Other"
        })]
        )
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        firstName.isEnabled = false
        lastName.isEnabled = false
        gender.isEnabled = false
        phone.isEnabled = false
        email.isEnabled = false
        genderTF.isEnabled = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func BackToHome(_ sender: UIBarButtonItem) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
        
        
    }
    
   

    @IBOutlet weak var EditProfile: UIBarButtonItem!
    @IBAction func profile(_ sender: UIBarButtonItem) {
        
        firstName.isEnabled = true
        lastName.isEnabled = true
        gender.isEnabled = true
        phone.isEnabled = true
        email.isEnabled = true
        
        firstName.backgroundColor = UIColor.white
        lastName.backgroundColor = UIColor.white
        gender.backgroundColor = UIColor.white
        email.backgroundColor = UIColor.white
        
        EditProfile.title = "ADD"

       
        
        
        
    }
    
}
