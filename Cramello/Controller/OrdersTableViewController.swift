//
//  OrdersTableViewController.swift
//  Cramello
//
//  Created by Adam on 12/7/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class OrdersTableViewController: UITableViewController  , NVActivityIndicatorViewable{
    
    @IBOutlet weak var orderTable: UITableView!
    
    var   allOrders = [OrderModel]()
    
    
    @IBAction func menu(_ sender: UIButton) {
        

        let size = CGSize(width: 30, height: 30)
        
        startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: (sender as AnyObject).tag)!)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage("Loading ...")
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.stopAnimating()
        }
        
        let menu  = self.storyboard?.instantiateViewController(withIdentifier: "menuContainer") as! ContainerVC
        self.present(menu, animated: true , completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
  
        
        getMyOrders()
     
    }
    
    @IBAction func BacktoHome(_ sender: UIBarButtonItem) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
        
    }
    func getMyOrders(){
        
        Web_Services.MyOrders { (error, success, allorders) in
          
            
            if allorders != nil {
                self.allOrders = allorders!
                self.orderTable.reloadData()
                print(" all orders  ::::::::  \(self.allOrders)")
            }
            else {
                print (error ?? 0)
            }
        }
    }



    // MARK: - Table view data source

 

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
          return allOrders.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "orederCell", for: indexPath) as! OrderCellVC
        
        
       
        
        let arrayOf_dataInJSON = allOrders[indexPath.row] 
        
        cell.orderDAte.text = arrayOf_dataInJSON.order_date
        
        cell.orderID.text?.append( arrayOf_dataInJSON.order_id!)    
     
    //    cell.orderName.text = String(describing: arrayOf_dataInJSON.products?[indexPath.row])
        
        
        cell.orderStatus.text = arrayOf_dataInJSON.order_status
        
        if cell.orderStatus.text == "processing" {
           cell.orderStatus.textColor = UIColor.red
            
            
        }
        else if cell.orderStatus.text == "pending"{
            cell.orderStatus.textColor = UIColor.orange
        }
        else {
            cell.orderStatus.textColor = UIColor.green
        }
        

  
        
        return cell
    }




}
