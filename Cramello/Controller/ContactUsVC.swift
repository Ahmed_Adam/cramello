//
//  ContactUsVC.swift
//  Cramello
//
//  Created by Adam on 12/4/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import MapKit



class ContactUsVC: UIViewController , MKMapViewDelegate {



    @IBOutlet weak var mapview: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapview.showsUserLocation = true
        mapview.delegate = self
      


    
    }


    @IBAction func zoom(_ sender: UIButton) {
        
        
        let location = mapview.userLocation
                let region = MKCoordinateRegionMakeWithDistance((location.location?.coordinate)!, 2000, 2000)
        
                mapview.setRegion(region, animated: true)
        
        
    }
    

    
    @IBAction func BackToHome(_ sender: UIBarButtonItem) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
        
        
    }


}



