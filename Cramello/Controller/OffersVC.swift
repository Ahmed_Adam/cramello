//
//  OffersVC.swift
//  Cramello
//
//  Created by Adam on 12/4/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire

class OffersVC: UIViewController {

    @IBOutlet var sliderView: ImageSlideshow!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        sliderView.backgroundColor = UIColor.white
        sliderView.slideshowInterval = 5.0
        sliderView.pageControlPosition = PageControlPosition.underScrollView
        sliderView.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        sliderView.pageControl.pageIndicatorTintColor = UIColor.black
        sliderView.contentScaleMode = UIViewContentMode.center
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        sliderView.activityIndicator = DefaultActivityIndicator()
        sliderView.currentPageChanged = { page in
            print("current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        
        sliderView.setImageInputs([
            ImageSource(image : #imageLiteral(resourceName: "best-birthday-cake-recipe-for-adults1_1-1")  ),
            ImageSource(image: #imageLiteral(resourceName: "001_home")),
            ImageSource(image: #imageLiteral(resourceName: "image")),
            ImageSource(image: #imageLiteral(resourceName: "02")),
            ImageSource(image: #imageLiteral(resourceName: "03"))
            ])
        
       // sliderView.setImageInputs(sdWebImageSource as! [InputSource])
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(OffersVC.didTap))
        sliderView.addGestureRecognizer(recognizer)
        
        
        

        // Do any additional setup after loading the view.
    }
    
    @objc func didTap() {
      //  let fullScreenController = sliderView.presentFullScreenController(from: self)
//        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
//        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    


    
    @IBAction func backtoHome(_ sender: UIBarButtonItem) {
        
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
    }
    
 

}
