

import UIKit
import LUExpandableTableView
import Alamofire

final class MenuViewController: UIViewController  {
    // MARK: - Properties
    
    @IBAction func details(_ sender: UITapGestureRecognizer) {
    }
    
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        
        let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
        self.present(home, animated: true, completion: nil)
        
    }
    
    
    private let sectionHeaderReuseIdentifier = "MySectionHeader"
    
    // MARK: - ViewController
    
  
    

    @IBOutlet weak var extableview: LUExpandableTableView!
    var   allCat = [CatogriesModel]()
    var    allProduct = [ProductModel]()
  
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        extableview.register(UINib(nibName: "MyExpandableTableViewSectionHeader", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: sectionHeaderReuseIdentifier)
        
        extableview.expandableTableViewDataSource = self
        extableview.expandableTableViewDelegate = self
        
        getCategories()
        
    }
    
    @IBAction func onMore(_ sender: UIBarButtonItem) {
        
        print("TOGGLE SIDE MENU")
        NotificationCenter.default.post(name: NSNotification.Name("ToggleSideMenu"), object: nil)
        
    }
    
    func  getCategories(){
     
        Web_Services.GetCatogries { (error, response , catArray ) in
            
            if catArray != nil {
                self.allCat = catArray!
                
               // self.allProduct = self.allCat[0].products!
                self.extableview.reloadData()
              
                print(" all cat   ::::::::  \(self.allCat)")
            }
            else {
                print (error ?? 0)
            }
            
            
        }
     
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        

    }
}

// MARK: - LUExpandableTableViewDataSource

extension MenuViewController: LUExpandableTableViewDataSource {
    func numberOfSections(in extableview: LUExpandableTableView) -> Int {
        return allCat.count
    }
    
    func expandableTableView(_ extableview: LUExpandableTableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: extableview.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.blue
        return footerView
    }
    
    func expandableTableView(_ extableview: LUExpandableTableView, heightForFooterInSection section: Int) -> CGFloat {
        return 40
    }
    
    func expandableTableView(_ extableview: LUExpandableTableView, numberOfRowsInSection section: Int) -> Int {
        return (allCat[section].products?.count)!
    }
    
    func expandableTableView(_ extableview: LUExpandableTableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = extableview.dequeueReusableCell(withIdentifier: "MyCell") as? MyTableViewCell
        let product = allCat[indexPath.section].products![indexPath.row]
        
        cell?.productName.text = product.name
        cell?.productSalary.text = product.price
        cell?.orderStatuts.text = product.is_saleable
        
        Alamofire.request( product.image_url! ).responseData { (response ) in
            
            cell?.productPic.image = UIImage(data: response.data!)
        }
        return cell!
    }
    
    func expandableTableView(_ extableview : LUExpandableTableView, sectionHeaderOfSection section: Int) -> LUExpandableTableViewSectionHeader {
        guard let sectionHeader = extableview.dequeueReusableHeaderFooterView(withIdentifier: sectionHeaderReuseIdentifier) as? MyExpandableTableViewSectionHeader else {
            assertionFailure("Section header shouldn't be nil")
            return LUExpandableTableViewSectionHeader()
        }
        
        let arrayOf_dataInJSON = allCat[section]
        
        
        sectionHeader.label.text = arrayOf_dataInJSON.name
        
        return sectionHeader
    }
}

// MARK: - LUExpandableTableViewDelegate

extension MenuViewController: LUExpandableTableViewDelegate {
    func expandableTableView(_ extableview: LUExpandableTableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 100
    }
    
    func expandableTableView(_ extableview: LUExpandableTableView, heightForHeaderInSection section: Int) -> CGFloat {
       
        return 69
    }
    
    
}


