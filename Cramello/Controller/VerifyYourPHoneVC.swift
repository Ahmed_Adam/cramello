//
//  VerifyYourPHoneVC.swift
//  Cramello
//
//  Created by Adam on 12/3/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import UIKit
import DropDown
import CountryPicker


class VerifyYourPHoneVC: UIViewController,CountryPickerDelegate   {
    
    
    
    
   
    

    @IBOutlet weak var codeTextField: UITextField!
    var  picker = CountryPicker()

    @IBOutlet weak var countryImage : UIImageView!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
   
 
   


    @IBAction func userRegister(_ sender: UIButton) {
        
        let phone  = codeTextField.text! + phoneTextField.text!
        if (phone.isEmpty) {
            print ("you must write phone ")
            return
        }
        
        Web_Services.register(phone: phone) { (error, success,isRegiatered) in
            
            if !isRegiatered && success {
                
                    
                    let EnterCode = self.storyboard?.instantiateViewController(withIdentifier: "EnterCode") as! EnterCodeVC
                    self.present(EnterCode, animated: true, completion: nil)
                    
                }
                else if isRegiatered && success {
                
                       let home = self.storyboard?.instantiateViewController(withIdentifier: "Container") as! ContainerVC
                       self.present(home, animated: true, completion: nil)
            }
           
        }
  }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    codeTextField.inputView = picker
  picker.countryPickerDelegate = self
        
    }
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        
        self.codeTextField.text = phoneCode
        self.countryImage.image = flag
        
    }

}
