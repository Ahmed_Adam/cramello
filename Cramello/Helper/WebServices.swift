//
//  WebServices.swift
//  Cramello
//
//  Created by Adam on 12/4/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON

class Web_Services: NSObject {
    
    
    static var shared = Web_Services()
    
    
    
    class func GetArea ( complition :  @escaping (_ error:Error? ,_ success: Bool , _ allAreas :[String]? )->Void){
        
        let url = "http://cramello.com/en/api_v2/address/area"
       var areaArray = [""]
        Alamofire.request(url).responseJSON { (response ) in
            
            switch response.result
            {
            case .failure(let error):
                print(error)
            case .success(let value):
                print (value)
                let json = JSON(value)
                let dataJesonArray  =  json["model"].array
                
                for data in dataJesonArray! {
                    areaArray.append(data.string!)
                print(data)
                }
        }
            complition(nil , true , areaArray  )
    }
}
    
    class func MyAddressList (customer_code : Int  , complition :  @escaping (_ error:Error? ,_ success: Bool , _ allAddress :[AddressListModel]?)->Void){
        
        let url = URLs.AddressList
        var addressArray = [AddressListModel]()
        let parameters = [ "customer_id": customer_code ]
        
        //        let postString = "{\"mobile\":\"+201126223344\"}"
        

        
        Alamofire.request(url, method: .post, parameters: parameters , encoding: JSONEncoding.default ).responseJSON {response in
            
            switch response.result {
            case .success(let value):
                if let result = response.result.value {
                    print("JSON: \(result)")
                    
                    
                    let json = JSON(value)
                    let dataJesonArray  =  json["model"].array
                    for data in dataJesonArray! {
                        guard let data = data.dictionary else {return}
                        let address = AddressListModel(dictionary: json )

                        address?.customer_id = data["customer_id"]?.string
                        address?.id = data["id"]?.string
                        address?.addressName = data["addressName"]?.string
                        address?.area = data["area"]?.string
                        address?.addressType = data["addressType"]?.string
                        address?.block = data["block"]?.string
                        address?.street = data["street"]?.string
                        address?.apartmentNo = data["street"]?.string
                        address?.buildingNo = data["buildingNo"]?.string
                        address?.floor = data["floor"]?.string
                        address?.mobile = data["mobile"]?.string
                        address?.extraDeleveryInformation = data["extraDeleveryInformation"]?.string
                        address?.is_default_billing = data["is_default_billing"]?.bool
                        address?.is_default_shipping = data["is_default_shipping"]?.bool
                        addressArray.append(address!)
                       
                        
                        print ("HI from webservice ")
                    }
                    complition(nil, true, addressArray)
                    
                }
            case .failure(let error):
                print(error)
                
            }
        }
    }
    
    
    
    class func register (phone: String  , complition :   @escaping (_ error:Error? ,_ success: Bool, _ isRegistered:Bool) -> Void){
    
        let url = URLs.register
        
        let parameters = [ "mobile": phone ]
        
//        let postString = "{\"mobile\":\"+201126223344\"}"
        
        Alamofire.request(url, method: .post, parameters: parameters , encoding: JSONEncoding.default ).responseJSON {response in

            switch response.result {
            case .success(let value):

                    
                 var json = JSON(value)
                    
                    
                    if json["msg"].string == "There is already an account with this mobile number"
                    {
                        let url = URLs.login
                        let parameters = [ "mobile": phone ]
                        let urlComponent = URLComponents(string: url)!
                        let headers = [ "Content-Type": "application/json" ]
                        var request = URLRequest(url: urlComponent.url!)
                        request.httpMethod = "POST"
                        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
                        request.allHTTPHeaderFields = headers
                        
                        
                        Alamofire.request(request).responseJSON { (response) in
                                
                                switch response.result {
                                case .success(let value ):
                                    print("JSON: \(value)")
                                    let json = JSON(value)
                                    if let id  =  json["model"]["id"].string {
                                        USER_ID.save_User_ID(user_id: "\(id)")
                                          complition(nil, true, true)
                                    }
                                    
                                    if json["msg"].string == "User not verified" {
                                        
                                        complition(nil, true, false)
                                    }
                                    
                                    print ("hi after datajsonArray  array" )
       
                                  case .failure (let error):
                                    print(error)
                                    
                                }
                            }
                        }
                    else {
                                 print(json)
                        if let id  =  json["model"]["id"].string {
                            USER_ID.save_User_ID(user_id: "\(id)")
                            complition(nil, true, true)
                        }
                        
                               print ("HI from webservice ")
                        
                      
                    }

                    
                   
            
            case .failure(let error):
                print(error)

            }
        }
    }
    
    class func Verify (code : String , mobile : String  , complition :  @escaping (_ error:Error? ,_ success: Bool)->Void){
        
        let url = URLs.UserVerify
        let parameters = [ "mobile": mobile  , "code" : code ]
        
        Alamofire.request(url, method: .post, parameters: parameters  , encoding: JSONEncoding.default ).responseJSON {response in
            
            switch response.result {
            case .success(_):
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                }
            case .failure(let error):
                print(error)
                
            }
        }
    }
    
    
    class func MyOrders ( complition :  @escaping (_ error:Error? ,_ success: Bool ,_ allCat :[OrderModel]?)->Void){
       let url = URLs.orderList
        var orderArr = [OrderModel]()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result
            {
            case .failure(let error):
                print(error)
            case .success(let value):
                print (value)
                
                let json = JSON(value)
             let dataJesonArray  =  json["model"].array
                print ("hi after datajsonArray  array" )

                for data in dataJesonArray! {
                    guard let data = data.dictionary else {return}
                    let order = OrderModel(dictionary: json)
                    order?.grand_total = data["grand_total"]?.double
                    order?.order_date = data["order_date"]?.string
                    order?.order_id = data["order_id"]?.string
                    order?.order_status = data["order_status"]?.string
                    order?.order_invoice = data["order_invoice"]?.int
                    orderArr.append(order!)
                    print ("HI from webservice ")
                }
                complition(nil, true, orderArr)
            }
        }
    }
    

    class func CreateAddress (address :  [String:String] , complition :  @escaping (_ error:Error? ,_ success: Bool )->Void){
      //  let url = URLs.AddressCreat
        
        
        

        let parameters = [   "id" :  address["id"] ,
                             "addressName" : address["addressName"],
                             "addressType" : address["addressType"] ,
                             "apartmentNo" : address["appartNo"],
                             "area"        : address["area"],
                             "street"      : address["street"],
                             "block"       : address["block"] ,
                             "buildingNo"  : address["buildingNo"],
                             "extraDeleveryInformation" : address["extraDeleveryInformation" ],
                             "floor"       : address["floor"],
                             "mobile"      : address["mobile"] ]

         let url =  URLs.AddressCreat
        
        var urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        
        Alamofire.request(request).responseJSON { (response) in
            switch response.result {
            case .success(_):
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                }
            case .failure(let error):
                print(error)
                
            }
       
       }
     complition(nil ,true)
    }
    
    
    class func MyOrdersDetails ( complition :  @escaping (_ error:Error? ,_ success: Bool ,_ orderDetails :[OrderDetailsModel]?)->Void){
        let url = URLs.OrdersDetails
        let orderDetailsArr = [OrderDetailsModel]()
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result
            {
            case .failure(let error):
                print(error)
            case .success(let value):
                print (value)
//
//                let json = JSON(value)
//                let dataJesonArray  =  json["billing_address"].array
//                print ("hi after datajsonArray  array" )
//
//                for data in dataJesonArray! {
//                    guard let data = data.dictionary else {return}
//                    let order_details = OrderDetailsModel(dictionary: json)
//                    order_details?.order_id = data["order_id"]?.int
//                    order_details?.address?.firstname = data["order_date"]?.string
//                    order_details?.address?.street = data["order_id"]?.string
//                    order_details?.address?.tel = data["order_id"]?.int
//                   // order_details?.order = data["order_id"]?.int
//                    orderDetailsArr.append(order_details!)
                    print ("HI from webservice ")
                }
                complition(nil, true, orderDetailsArr)
            }
        //}
    }
    
    
    
//    class func MyOrders ( complition :  @escaping (_ error:Error? ,_ success: Bool ,_ allCat :[OrderModel]?)->Void){
//        let url = URLs.orderList
//        var orderArr = [OrderModel]()
    
    
    class func GetCatogries  ( complition :  @escaping (_ error:Error? ,_ success: Bool , _ allCat :[CatogriesModel]?)->Void){
        
        let url = URLs.CategoriesTree
        var catArr = [CatogriesModel]()
        
        Alamofire.request(url).responseJSON { (response ) in
            
            switch response.result
            {
            case .failure(let error):
            
                print(error)
                
                
            case .success(let value):
                
                print (value)
                
                let json = JSON(value)
                let dataJesonArray  =  json["model"].array
                print ("hi after datajsonArray  array" )
                
                for data in dataJesonArray! {
                    guard let data = data.dictionary else {return}
                    var category = CatogriesModel(dictionary: json)
                    
                    category?.name = data["name"]?.string
                    
                    var allproducts = [ProductModel]()
                    
                    let productArray = data["products"]?.array
//                    for pro in productArray{
//
//                        var pro =
//                    }
                           //  = data["products"]?.arrayObject
                            for product in productArray! {
                      //          guard let product = product.dictionary else {return}
                                let pro = ProductModel(dictionary: product)
                                print(pro!)
                                category?.products?.append(pro!)
                            //    pro?.name = product["name"]?.string
                            }
                    catArr.append(category!)
                    print ("HI from webservice ")
                }
                complition(nil, true, catArr)
            }
        }
    }
    
    
    
    class func GetOffers  ( complition :  @escaping (_ error:Error? ,_ success: Bool , _ allOffers :[OffersModel]?)->Void){
        
        let url = URLs.Offers
        var offersArr = [OffersModel]()
//
//        "type":"offers",
//        "page":1,
//        "limit":10
        let parameters = [   "type" :  "offers",
                             "page" : "1",
                             "limit" :"10"   ]
    
        var urlComponent = URLComponents(string: url)!
        let headers = [ "Content-Type": "application/json" ]
        var request = URLRequest(url: urlComponent.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters)
        request.allHTTPHeaderFields = headers
        
        
        Alamofire.request(request).responseJSON { (response) in
            switch response.result {
            case .success(let value ):
                
                    print("JSON: \(value)")
                    
                    let json = JSON(value)
                    let dataJesonArray  =  json["model"].array
                    print ("hi after datajsonArray  array" )
                    
                    for data in dataJesonArray! {
                        guard let data = data.dictionary else {return}
                        let offer = OffersModel(dictionary: json)
                        
                        offer?.imageUrl = data["image_url"]?.string
                        print(offer?.imageUrl ?? "1234")
                        
                        offersArr.append(offer!)
                        print ("HI from webservice ")
                    }

            case .failure(let error):
                print(error)
                
                
            }
            complition(nil, true, offersArr)
        }
    
    }
    
}
    

