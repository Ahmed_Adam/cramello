//
//  HelperData.swift
//  Cramello
//
//  Created by Adam on 12/4/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation


class HelperData: NSObject {
    
    class func saveAPIToken (token:String){
        let def = UserDefaults.standard
        def.setValue(token , forKey: "api_token")
        def.synchronize()
    }
    class  func getAPIToken ()->String? {
        let dif = UserDefaults.standard
        return (dif.object(forKey : "api_token") as? String)
    }
    
}
