//
//  Config.swift
//  Cramello
//
//  Created by Adam on 12/4/17.
//  Copyright © 2017 aldar-int. All rights reserved.
//

import Foundation



struct URLs {
    
    
    static let Countries = "http://cramello.com/en/api_v2/address/country"
    
    
        //   User
    
    
    static let login = "http://cramello.com/en/api_v2/mobile/login"
    static let register = "http://cramello.com/en/api_v2/mobile/register"
    
    static let UserForgetPassword = "http://cramello.com/en/api_v2/customer/forgot"
    static let UpdatePassword = "http://cramello.com/en/api_v2/customer/updatepass"
    
    static let UserVerify = "http://cramello.com/en/api_v2/mobile/verify"
    static let ResendVerifyCode = "http://cramello.com/en/api_v2/mobile/code"
    static let SendSMS = "https://api.twilio.com/2010-04-01/Accounts/ACbdbd77c0080a3208aea2211d54edbe8b/Messages.json"
    
    static let UserStatus = "http://cramello.com/en/api_v2/customer/status"
    static let UserEdit = "http://cramello.com/en/api_v2/customer/update"
  
    // **************************************** //
    

    // Address URLs
    
    static let AddressCreat = "http://cramello.com/en/api_v2/address/create"
    static let AddressStatus = "http://cramello.com/en/api_v2/address/status"
    static let AddressEdit = "http://cramello.com/en/api_v2/address/edit"
    static let AddressList = "http://cramello.com/en/api_v2/address/getlist"
    static let AddressDelete = "http://cramello.com/en/api_v2/address/delete"
    
    // **************************************** //
    
    
    // products
    
    static let CategoriesTree = "http://cramello.com/en/api_v2/index/categories"

    static let MostSelling = "http://cramello.com/en/api_v2/index/products"
    static let Offers = "http://cramello.com/en/api_v2/index/products"
    static let Products = "http://cramello.com/en/api_v2/products/get"
    static let ProductDetails = "http://cramello.com/en/api_v2/products/detail/3"
    
    
    // **************************************** //
    
    
    // Cart URLs
    
    static let CartList = "http://cramello.com/en/api_v2/cart/list"
    static let CratAddProduct = "http://cramello.com/en/api_v2/cart/add"
    static let CartRemove = "http://cramello.com/en/api_v2/cart/remove"
    static let CartUpdateProduct = "http://cramello.com/en/api_v2/cart/update"
    
    
    
    // **************************************** //
    
    
    // Orders URLs

    static let orderList = "http://cramello.com/en/api_v2/order/list/80"
    static let OrdersDetails = "http://cramello.com/en/api_v2/order/get/100000005"
    static let OrdersCreate = "http://cramello.com/en/api_v2/order/create"
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
